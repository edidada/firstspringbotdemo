# firstspringbotdemo

## idea 新建SpringBot项目

[参考网址](https://www.cnblogs.com/weizaibug/p/6657077.html)

#### 1、打开idea，选择Create New Project
#### 2、项目配置，选择Spring Initializr，Project SDK选择1.8，URL选择默认，不做修改。如下图所示，然后选择Next
#### 3、继续项目配置
#### 4、Spring Boot 版本选择，我这里选择了1.5.2，只要勾选Web下的web就可以了，然后Next
#### 5、项目名称和路径，填写完后Finish

项目新建完成

### 添加网址，完成特定网页
- 新建类HelloController
- 完成编码

### 运行，测试网页效果
- idea自带tomcat服务器
- 选中Application对应的类，右键，选择 “Run”，控制台显示已启动8080端口
- 访问127.0.0.1网页，查看效果

#### 备注
菜单“File”->“Project Struc...”，搜索“Maven”，User setting xml 对应的文件，更换成本仓库根目录下的settings.xml文件，此已设置国内的阿里镜像，可以加快下载jar包的速度。


package com.example.firstspringbotdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstspringbotdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstspringbotdemoApplication.class, args);
	}
}
